package com.hewl.job;

import com.hewl.entity.TestEntity;
import com.hewl.utils.RestTemplateUtil;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
//持久化
@PersistJobDataAfterExecution
//禁止并发执行(Quartz不要并发地执行同一个job定义（这里指一个job类的多个实例）)
@DisallowConcurrentExecution
public class MyJob extends QuartzJobBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyJob.class);

    // private boolean httpRequest = false;                        // 布尔类型的变量，不要加is前缀

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        String url = "http://localhost:9002/quartz/rest/getEntity";

        TestEntity entity = RestTemplateUtil.getRequest(url, TestEntity.class);
        LOGGER.info("entity = {}", entity);
        System.out.println("MyJob...");
    }
}
