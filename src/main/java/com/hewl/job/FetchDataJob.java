package com.hewl.job;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.hewl.entity.User;
import com.hewl.mapper.UserMapper;

import java.util.Random;
import java.util.stream.IntStream;
//持久化
@PersistJobDataAfterExecution
//禁止并发执行(Quartz不要并发地执行同一个job定义（这里指一个job类的多个实例）)
@DisallowConcurrentExecution
public class FetchDataJob extends QuartzJobBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchDataJob.class);

    @Autowired
    private UserMapper userMapper;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

        // TODO 业务处理

        Random random = new Random();
        IntStream intStream = random.ints(18, 100);
        int first = intStream.limit(1).findFirst().getAsInt();
        int count = userMapper.saveUser(new User("zhangsan" + first, first));
        if (count == 0) {
            LOGGER.error("用户保存失败！");
            return;
        }
        LOGGER.info("用户保存成功");
    }
}