package com.hewl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskSchedulingClusterApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskSchedulingClusterApplication.class, args);
	}

}
