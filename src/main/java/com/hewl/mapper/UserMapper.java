package com.hewl.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.hewl.entity.User;

@Mapper
public interface UserMapper {

    int saveUser(User user);
}
